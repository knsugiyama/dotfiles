#!/bin/bash

fish -C 'fisher add oh-my-fish/theme-bobthefish;'
fish -C 'fisher add jethrokuan/z;'
fish -C 'fisher add jethrokuan/fzf;'
fish -C 'fisher add decors/fish-ghq;'
