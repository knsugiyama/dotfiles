#!/bin/bash

sudo sed -i.bak -e "s/http:\/\/archive\.ubuntu\.com/http:\/\/jp\.archive\.ubuntu\.com/g" /etc/apt/sources.list

# =====
# clone dotfiles
# =====
if [ -e ~/dotfiles ]; then
  rm -rf ~/dotfiles
fi
# git clone -b $branchName https://github.com/knsugiyama/dotfiles.git ~/dotfiles
git clone -b master https://knsugiyama@bitbucket.org/knsugiyama/dotfiles.git ~/dotfiles

sudo apt update
sudo apt upgrade -y

find dotfiles/ -type f -exec chmod 777 \{\} \;

# =====
# start symbolic link shell
# =====
~/dotfiles/setup/link.sh

echo '====================================== install linixbrew ======================================'
sudo apt install build-essential -y
sh -c "$(curl -fsSL https://raw.githubusercontent.com/Linuxbrew/install/master/install.sh)"
test -d ~/.linuxbrew && eval $(~/.linuxbrew/bin/brew shellenv)
test -d /home/linuxbrew/.linuxbrew && eval $(/home/linuxbrew/.linuxbrew/bin/brew shellenv)
test -r ~/.bash_profile && echo "eval \$($(brew --prefix)/bin/brew shellenv)" >>~/.bash_profile
echo "eval \$($(brew --prefix)/bin/brew shellenv)" >>~/.profile

echo '====================================== wsl base setup ======================================'
source ~/.bashrc
source .profile
~/dotfiles/setup/wsl.sh

echo '====================================== install utility ======================================'
~/dotfiles/setup/common.sh
echo '====================================== run brew ======================================'
~/dotfiles/setup/brew.sh
echo '====================================== install backend modules ======================================'
~/dotfiles/setup/backend.sh
echo '====================================== install frontend modules ======================================'
~/dotfiles/setup/frontend.sh
echo '====================================== install tools ======================================'
~/dotfiles/setup/tools.sh
echo '====================================== install asciidoctor ======================================'
~/dotfiles/setup/asciidoctor.sh
echo '====================================== install fisher plugins ======================================'
~/dotfiles/fish/fish_plug.sh
echo '====================================== install tmux plugins ======================================'
~/dotfiles/tmux/tmux_plug.sh
