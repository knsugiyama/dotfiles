#!/bin/bash

brew install gcc
brew install fish
brew install fzf
brew install tmux
brew install peco
brew install make
brew install jq
brew install shellcheck
brew install tree
brew install ren
brew install direnv
brew install wget
brew install tig
brew install hub
brew install rg
brew install ghq
brew install asciidoc
brew install graphviz
brew install plantuml
brew install git-credential-manager
brew install ruby
bewq install anyenv

# nodenvなどはanyenvを使ってインストールする
# anyenv install --init
# anyenv install nodenv
