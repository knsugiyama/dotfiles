#!/bin/bash

# to build ren and tree command and make vimproc
sudo apt -y update
sudo apt -y upgrade

# japanease lang setting
sudo apt -y install language-pack-ja
sudo update-locale LANG=ja_JP.UTF-8
timedatectl set-timezone Asia/Tokyo
sudo apt -y install manpages-ja manpages-ja-dev
sudo locale-gen ja_JP.UTF-8

# xserver tools
sudo apt install -y xauth x11-apps
sudo apt install -y fcitx-mozc fonts-noto-cjk fonts-noto-color-emoji dbus-x11

sudo sh -c "dbus-uuidgen > /var/lib/dbus/machine-id"
sudo im-config -n fcitx

## Mozcの設定を開くコマンド
## fcitx-configtool # 入力メソッド設定
## /usr/lib/mozc/mozc_tool --mode=config_dialog # Mozc自体の設定

# install Cica font
wget -P /tmp/Cica https://github.com/miiton/Cica/releases/download/v5.0.1/Cica_v5.0.1_with_emoji.zip
unzip -d /tmp/Cica /tmp/Cica/Cica_v5.0.1_with_emoji.zip
rm -f /tmp/Cica/Cica_v5.0.1_with_emoji.zip
sudo cp -r /tmp/Cica /usr/share/fonts/truetype/

# 日本時間に変更する
# /etc/systemd/timesyncd.conf に
# NTP=ntp.nict.jp を記載する
