mkdir -p ~/opt
# eclipse
wget http://ftp.yz.yamagata-u.ac.jp/pub/eclipse/technology/epp/downloads/release/2019-09/R/eclipse-java-2019-09-R-linux-gtk-x86_64.tar.gz
sudo tar -zxvf eclipse-java-2019-*-R-linux-gtk-x86_64.tar.gz -C ~/opt
sudo ln -s ~/opt/eclipse/eclipse /usr/bin/eclipse

# IntelliJ IDEA
wget https://download.jetbrains.com/idea/ideaIC-2019.2.4-no-jbr.tar.gz
mkdir ~/opt/ideaIC && sudo tar -zxvf ideaIC-2019.*.tar.gz -C ~/opt/ideaIC --strip-components 1
sudo ln -s ~/opt/ideaIC/bin/idea.sh /usr/bin/idea

# chrome
curl https://dl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
echo 'deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main' | sudo tee /etc/apt/sources.list.d/google-chrome.list
sudo apt update
sudo apt -y install google-chrome-stable

# ngrok
wget https://bin.equinox.io/c/4VmDzA7iaHb/ngrok-stable-linux-amd64.zip
unzip ngrok-stable-linux-amd64.zip -d ~/opt
sudo cp ~/opt/ngrok /usr/bin/
