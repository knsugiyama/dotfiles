#!/bin/bash

mkdir -p ~/.config/fish
mkdir ~/.byobu/

# link dotfiles
ln -sf ~/dotfiles/git/.gitconfig ~/.gitconfig
ln -sf ~/dotfiles/fish/config.fish  ~/.config/fish/config.fish
ln -sf ~/dotfiles/.bashrc ~/.bashrc
ln -sf ~/dotfiles/package-update.sh ~/package-update.sh
ln -sf ~/dotfiles/tmux/.tmux.conf ~/.tmux.conf
ln -sf ~/dotfiles/tmux/.tmux.conf ~/.byobu/.tmux.conf
