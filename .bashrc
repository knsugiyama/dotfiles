export PS1='\[\e[1;33m\]\u@\h \w ->\n\[\e[1;36m\] \@ \d\$\[\e[m\] '

# path settigs
PATH=/home/linuxbrew/.linuxbrew/bin:$PATH

# asciidoc
export XML_CATALOG_FILES="/home/linuxbrew/.linuxbrew/etc/xml/catalog"

# 日本語入力設定
# 起動時に実行するようにする
## WSL2
export DISPLAY=$(cat /etc/resolv.conf | grep nameserver | awk '{print $2}'):0
export TZ="Asia/Tokyo"
export LANG="ja_JP.UTF-8"
export LC_ALL="ja_JP.UTF-8"
export GTK_IM_MODULE=fcitx
export QT_IM_MODULE=fcitx
export XMODIFIERS="@im=fcitx"
export DefaultIMModule=fcitx
xset -r 49 # 半角全角点滅防止
fcitx-autostart

# JAVA
JAVA_HOME=$(readlink -f /usr/bin/javac | sed "s:/bin/javac::")
export JAVA_HOME
PATH=$PATH:$JAVA_HOME/bin

export PATH
